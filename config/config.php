<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_literature
 * @license    LGPL
 * @filesource
 */

/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['BE_MOD'], 1, array('simple_literature' => array()));

$GLOBALS['BE_MOD']['simple_literature']['simple_literature_categories'] = array
(
    'tables' => array('tl_simple_literature_categories'),
    'stylesheet' => 'system/modules/simple_literature/assets/css/be.css',
    'icon'   => 'system/modules/simple_literature/assets/icons/category.png'
);

$GLOBALS['BE_MOD']['simple_literature']['simple_literature_entries'] = array
(
    'tables' => array('tl_simple_literature_entries','tl_simple_literature_properties'),
    'stylesheet' => 'system/modules/simple_literature/assets/css/be.css',
    'icon'   => 'system/modules/simple_literature/assets/icons/cutlery.png'
);


/**
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['FE_MOD'], 2, array
(
    'literature' => array
    (
        'simple_literature_list'    => '\Srhinow\SimpleLiterature\Modules\Frontend\ModuleSimpleLiteratureList',
        'simple_literature_details'    => '\Srhinow\SimpleLiterature\Modules\Frontend\ModuleSimpleLiteratureDetails',
    )
));

$GLOBALS['TL_MODELS']['tl_simple_literature_categories'] = \Srhinow\SimpleLiterature\Models\SimpleLiteratureCategoriesModel::class;
$GLOBALS['TL_MODELS']['tl_simple_literature_entries'] = \Srhinow\SimpleLiterature\Models\SimpleLiteratureEntriesModel::class;
$GLOBALS['TL_MODELS']['tl_simple_literature_properties'] = \Srhinow\SimpleLiterature\Models\SimpleLiteraturePropertiesModel::class;

/**
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_HOOKS']['getSearchablePages'][] = array('Srhinow\Simpleliterature\Hooks\SimpleLiteratureHooks', 'getSearchablePages');

/**
 * -------------------------------------------------------------------------
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 * -------------------------------------------------------------------------
 */
// $GLOBALS['TL_PERMISSIONS'][] = 'simple_literature_modules';
$GLOBALS['BM']['PROPERTIES']['ID'] = 1;


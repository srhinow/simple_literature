<?php
namespace Srhinow\SimpleLiterature\Modules\Frontend;

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_literature
 * @license    LGPL
 * @filesource
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */

use Contao\FrontendTemplate;
use Contao\Module;
use Contao\PageModel;
use Contao\StringUtil;
use Srhinow\SimpleLiterature\Models\SimpleLiteratureEntriesModel;


/**
 * Class ModuleSimpleLiterature
 */
abstract class ModuleSimpleLiterature extends Module
{
	/**
	 * Parse an item and return it as string
	 * @param object
	 * @param string
	 * @param integer
	 * @return string
	 */
	protected function parseEntry($objEntry, $strClass='', $intCount=0)
	{
		global $objPage;

		$objTemplate = new FrontendTemplate($this->item_template);
		$objTemplate->setData($objEntry->row());
		$objTemplate->class = (($objEntry->cssClass != '') ? ' ' . $objEntry->cssClass : '') . $strClass;

        // Add an image
        if ($objEntry->addImage && $objEntry->cover != '')
        {
            $objModel = \FilesModel::findByUuid($objEntry->cover);

            if ($objModel === null)
            {
                if (!\Validator::isUuid($objEntry->cover))
                {
                    $objTemplate->text = '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
                }
            }
            elseif (is_file(TL_ROOT . '/' . $objModel->path))
            {
                // Do not override the field now that we have a model registry (see #6303)
                $arrEntry = $objEntry->row();

                // Override the default image size
                if ($this->imgSize != '')
                {
                    $size = StringUtil::deserialize($this->imgSize);

                    if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]))
                    {
                        $arrEntry['size'] = $this->imgSize;
                    }
                }

                $arrEntry['singleSRC'] = $objModel->path;
                $this->addImageToTemplate($objTemplate, $arrEntry);
            }
        }

		//Detail-Url
		if($this->jumpTo)
		{
			$objDetailPage = PageModel::findByPk($this->jumpTo);
			$objTemplate->detailUrl = ampersand( $this->generateFrontendUrl($objDetailPage->row(),'/'.$objEntry->alias) );
			$objTemplate->teaser = StringUtil::substr($objEntry->preparation,100);
		}

		
		return $objTemplate->parse();
	}


	/**
	 * Parse one or more items and return them as array
	 * @param object
	 * @return array
	 */
	protected function parseLiterature($objLiterature)
	{
		$limit = $objLiterature->count();

		if ($limit < 1)
		{
			return array();
		}

		$count = 0;
		$arrLiterature = array();

		while ($objLiterature->next())
		{
			$arrLiterature[] = array
			(
				'data' => $objLiterature->row(),
				'html' => $this->parseEntry($objLiterature, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count)
			);
		}

		ksort($arrLiterature);
		return $arrLiterature;
	}
}

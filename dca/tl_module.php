<?php
/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2016 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['simple_literature_list'] 		= '{title_legend},name,headline,type;{category_legend},category;{config_legend},jumpTo,numberOfItems,perPage;{template_legend:hide},item_template,imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['simple_literature_details'] 		= '{title_legend},name,headline,type;{config_legend},imgSize;{template_legend:hide},item_template;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

/**
 * Fields
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['category'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['category'],
	'exclude'                 => true,
	'inputType'               => 'select',
    'foreignKey'              => 'tl_simple_literature_categories.title',
	'eval'                    => array('tl_class'=>'w50','includeBlankOption'=>true),
	'sql'                     => "varchar(32) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['item_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['item_template'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_simple_recipes', 'getItemTemplates'),
	'eval'                    => array('tl_class'=>'clr w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

/**
 * Class tl_module_simple_recipes
 * @package    simple_recipes
 */
class tl_module_simple_recipes extends Backend
{
	/**
	 * Return all news templates as array
	 * @return array
	 */
	public function getItemTemplates()
	{
		return $this->getTemplateGroup('item_simple_literature_');
	}
	
}

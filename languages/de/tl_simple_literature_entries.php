<?php
/**
 * PHP version 5
 * @copyright  sr-tag.de 2019
 * @author     Sven Rhinow
 * @package    simple_literature
 * @license    LGPL
 * @filesource
 */

/**
 * Global Operations
 */
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['properties'][0] = 'Einstellungen';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['properties'][1] = 'Alle Einstellungen zu den Literaturen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['maintitle'][0]         = 'Haupt-Titel';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['maintitle'][1]         = 'geben sie den Haupt-Titel für das Literatur an.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['subtitle'][0]         = 'Unter-Titel';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['subtitle'][1]         = 'geben sie den Unter-Titel für das Literatur an.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['alias'][0]			= 'Alias';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['alias'][1]        	= 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['categories'][0]			= 'Kategorien';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['categories'][1]        	= 'Ordnen Sie dem Literatur Kategorien zu.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['author'][0]			= 'Author';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['author'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['edition'][0]			= 'Edition';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['edition'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['year'][0]			= 'Erscheinungsjahr';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['year'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['isbn'][0]			= 'ISBN';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['isbn'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['link_amazon'][0]			= 'Link amazon';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['link_amazon'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['link_abebooks'][0]			= 'Link Abebooks';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['link_abebooks'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['description'][0]			= 'Beschreibung';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['description'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['addImage'][0]			= 'Literaturbild anlegen';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['addImage'][1]        	= 'Geben Sie hier an ob sie ein Literaturbild dem Literatur zuweisen möchten.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['cover'][0]			= 'Literaturbild';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['cover'][1]        	= 'wählen Sie hier da passende Bild zum Literatur.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['publisher'][0]			= 'Publisher';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['publisher'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['publisher'][0]			= 'Publisher';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['publisher'][1]        	= '';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['published'][0]			= 'Für Website-Besucher sichtbar';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['published'][1]        	= 'Geben sie hier ob das Literatur auf der Website angezeit werden soll.';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['new'][0]                          = 'Neues Literatur';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['new'][1]                          = 'Eine neues Literatur anlegen.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['edit'][0]                         = 'Literatur bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['edit'][1]                         = 'Literatur ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['copy'][0]                         = 'Literatur duplizieren';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['copy'][1]                         = 'Literatur ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['delete'][0]                       = 'Literatur löschen';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['delete'][1]                       = 'Literatur ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['show'][0]                         = 'Literaturdetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['show'][1]                         = 'Details für Literatur ID %s anzeigen.';

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['meta_legend']        	= 'Grund-Informationen';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['image_legend']        	= 'Bild zur Literatur';
$GLOBALS['TL_LANG']['tl_simple_literature_entries']['general_legend']        	= 'weitere Einstellungen';

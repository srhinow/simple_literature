<?php
namespace Srhinow\SimpleLiterature\Models;

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_literature
 * @license    LGPL
 * @filesource
 */

use Contao\Controller;
use Contao\Frontend;
use Contao\PageModel;
use Srhinow\SimpleLiterature\Models\SimpleLiteratureEntriesModel;
use Srhinow\SimpleLiterature\Models\SimpleLiteraturePropertiesModel;
use Srhinow\SimpleRecipes\Models\SimpleRecipesEntriesModel;

class SimpleLiteratureHooks extends Frontend
{
	/**
	 * Add store items to the indexer
	 *
	 * @param array   $arrPages
	 * @param integer $intRoot
	 * @param boolean $blnIsSitemap
	 *
	 * @return array
	 */
	public function getSearchablePages($arrPages, $intRoot=0, $blnIsSitemap=false)
	{
        $strBase = '';
		$props = SimpleLiteraturePropertiesModel::findPropertiesById(1);

		// Walk through each archive
		if ($props !== null)
		{
			// Skip if properties without target page
			if (!$props->jumpTo)
			{
				return $arrPages;
			}

			$itemsObj = SimpleRecipesEntriesModel::findAll();

			if($itemsObj !== null)
			{
				$objParent = PageModel::findWithDetails($props->jumpTo);

				// Set the domain (see #6421)
				$domain = ($objParent->rootUseSSL ? 'https://' : 'http://') . ($objParent->domain ?: \Environment::get('host')) . TL_PATH . '/';
				$strUrl = $domain . Controller::generateFrontendUrl($objParent->row(), ((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ?  '/%s' : '/items/%s'), $objParent->language);

				while($itemsObj->next())
				{
					// Link to the default page
					$arrPages[] = $strBase . sprintf($strUrl, (($itemsObj->alias != '' && !\Config::get('disableAlias')) ? $itemsObj->alias : $itemsObj->id));
				}

			}
		}

		return $arrPages;
	}

}

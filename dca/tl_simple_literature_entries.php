<?php

/**
 * PHP version 5
 * @copyright  sr-tag.de 2015
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * Table tl_simple_literature_entries
 */
$GLOBALS['TL_DCA']['tl_simple_literature_entries'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		// 'switchToEdit'                => true,
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
                'alias' => 'index',
                'start,stop,published' => 'index'
			)
		)
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 10,
            'fields'                  => array('categories'),
//			'flag'                    => 2,
            'panelLayout'             => 'filter;sort,search,limit',
		),
		'label' => array
		(
			'fields'                  => array('maintitle','subtitle'),
//            'label_callback'          => array('tl_simple_literature_entries', 'listEntries'),
		),
		'global_operations' => array
		(
			'properties' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['properties'],
				'href'                => 'table=tl_simple_recipes_properties&act=edit&id=1',
				'class'               => 'properties',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();"',
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif',
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif',
			)
		)
	),
	// Palettes
	'palettes' => array
	(
		'__selector__'                => array('addImage'),
		'default' => '{meta_legend},categories,maintitle,subtitle,alias,author,publisher,edition,year,isbn,link_amazon,link_abebooks,description;{image_legend},addImage,coverimage;{general_legend},published'
	),

	// Subpalettes
	'subpalettes' => array
	(
		'addImage'                    => 'cover',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
        'pid' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL"
        ),
        'sorting' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['sorting'],
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>3, 'tl_class'=>'w50'),
            'sql'					=> "varchar(255) NOT NULL default ''"
        ),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),

		'maintitle' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['maintitle'],
			'exclude'                 => true,
			'search'                  => true,
			'sorting'                 => true,
			'flag'                    => 1,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'maxlength'=>255,'tl_class'=>'long'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
        'subtitle' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['subtitle'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255,'tl_class'=>'long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
		'alias' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['alias'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'alnum', 'doNotCopy'=>true, 'spaceToUnderscore'=>true, 'maxlength'=>128),
			'sql'					=> "varchar(255) NOT NULL default ''",
			'save_callback' => array
			(
				array('tl_simple_literature_entries', 'generateAlias')
			)

		),
		'categories' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['categories'],
			'exclude'                 => true,
			'filter'                  => true,
			'search'                  => true,
//			'flag'                    => 1,
			'inputType'               => 'select',
			'foreignKey'              => 'tl_simple_literature_categories.title',
			'eval'                    => array('multiple'=>false),
            'sql'                     => "varchar(255) NOT NULL default ''",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'author' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['author'],
			'search'                  => true,
            'filter'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'edition' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['edition'],
			'search'                  => true,
            'filter'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'year' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['year'],
			'search'                  => true,
            'filter'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'isbn' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['isbn'],
			'search'                  => true,
            'filter'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'link_amazon' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['link_amazon'],
			'search'                  => true,
            'filter'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'link_abebooks' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['link_abebooks'],
			'search'                  => true,
            'filter'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'natural', 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
		),

		'description' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['description'],
			'filter'                  => false,
			'inputType'               => 'textarea',
			'eval'                    => array('mandatory'=>false, 'cols'=>'10','rows'=>'10','rte'=>'tinyMCE','tl_class'=>'clr'),
			'sql'					=> "blob NULL"

		),
		'addImage' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['addImage'],
			'exclude'                 => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'cover' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['cover'],
			'exclude'                 => true,
			'inputType'               => 'fileTree',
			'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr','extensions' => 'jpg,jpeg,gif,png'),
			'sql'                     => "binary(16) NULL"
		),
        'coverimage' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['coverimage'],
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255,'tl_class'=>'long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
		'publisher' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['publisher'],
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'published' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['published'],
			'exclude'                 => true,
			'filter'                  => true,
			'flag'                    => 2,
			'inputType'               => 'checkbox',
			'eval'                    => array('doNotCopy'=>true,'tl_class='=>'clr'),
			'sql'					=> "char(1) NOT NULL default ''"
		),
        'start' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['start'],
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 8,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'load_callback' => array
            (
                array('tl_simple_literature_entries', 'loadDate')
            ),
            'sql'                     => "varchar(10) NOT NULL default '0'"
        ),
        'stop' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_simple_literature_entries']['stop'],
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 8,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default '0'"
        )
	)
);


/**
 * Class tl_simple_literature_entries
 */
class tl_simple_literature_entries extends Backend
{
	
	/**
	 * Autogenerate an article alias if it has not been set yet
	 * @param mixed
	 * @param object
	 * @return string
	 */
	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;

		// Generate alias if there is none
		if (!strlen($varValue))
		{
			$autoAlias = true;
			$varValue = \StringUtil::standardize($dc->activeRecord->maintitle);
		}

		$objAlias = $this->Database->prepare("SELECT id FROM tl_simple_literature_entries WHERE id=? OR alias=?")
								   ->execute($dc->id, $varValue);

		// Check whether the page alias exists
		if ($objAlias->numRows > 1)
		{
			if (!$autoAlias)
			{
				throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
			}

			$varValue .= '-' . $dc->id;
		}

		return $varValue;
	}

    /**
     * List a particular record
     * @param $arrRow array
     * @return string
     */
    public function listEntries($arrRow)
    {
        $return = '<strong>'.$arrRow['headline'].'</strong> ';
        if((int) $arrRow['start'] > 0) $return .= date('d.m.Y',$arrRow['start']);
        if(((int) $arrRow['start'] > 0) && ((int) $arrRow['stop'] > 0)) $return .= ' - ';
        if((int) $arrRow['stop'] > 0) $return .= date('d.m.Y',$arrRow['stop']);

        return $return;
    }

    /**
     * Set the timestamp to 00:00:00 (see #26)
     *
     * @param integer $value
     *
     * @return integer
     */
    public function loadDate($value)
    {
        return strtotime(date('Y-m-d', $value) . ' 00:00:00');
    }
}

<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2018 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'Srhinow',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Library
	'Srhinow\SimpleLiterature\Hooks\SimpleLiteratureHooks'                    => 'system/modules/simple_literature/library/Hooks/SimpleLiteratureHooks.php',

	// Modules
	'Srhinow\SimpleLiterature\Modules\Frontend\ModuleSimpleLiterature'        => 'system/modules/simple_literature/modules/Frontend/ModuleSimpleLiterature.php',
	'Srhinow\SimpleLiterature\Modules\Frontend\ModuleSimpleLiteratureList'    => 'system/modules/simple_literature/modules/Frontend/ModuleSimpleLiteratureList.php',
	'Srhinow\SimpleLiterature\Modules\Frontend\ModuleSimpleLiteratureDetails' => 'system/modules/simple_literature/modules/Frontend/ModuleSimpleLiteratureDetails.php',

	// Models
	'Srhinow\SimpleLiterature\Models\SimpleLiteratureCategoriesModel'         => 'system/modules/simple_literature/models/SimpleLiteratureCategoriesModel.php',
	'Srhinow\SimpleLiterature\Models\SimpleLiteraturePropertiesModel'         => 'system/modules/simple_literature/models/SimpleLiteraturePropertiesModel.php',
	'Srhinow\SimpleLiterature\Models\SimpleLiteratureEntriesModel'            => 'system/modules/simple_literature/models/SimpleLiteratureEntriesModel.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'mod_simple_literature_entries_empty' => 'system/modules/simple_literature/templates/modules',
	'mod_simple_literature_list'          => 'system/modules/simple_literature/templates/modules',
	'mod_simple_literature_details'       => 'system/modules/simple_literature/templates/modules',
	'item_simple_literature_details'      => 'system/modules/simple_literature/templates/items',
	'item_simple_literature_list'         => 'system/modules/simple_literature/templates/items',
));

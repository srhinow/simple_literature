<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2016 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['simple_literature'] = array('Literaturen', 'Die einfache Literatur-Verwaltung');
$GLOBALS['TL_LANG']['MOD']['simple_literature_entries'] = array('Literatur-Einträge', 'Verwaltung der Literaturen.');
$GLOBALS['TL_LANG']['MOD']['simple_literature_categories'] = array('Literatur-Kategorien', 'Verwaltung der Literatur-Kategorien.');

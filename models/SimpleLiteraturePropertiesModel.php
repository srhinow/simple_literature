<?php
namespace Srhinow\SimpleLiterature\Models;

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_literature
 * @license    LGPL
 * @filesource
 */

use Contao\Model;

class SimpleLiteraturePropertiesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_literature_properties';

    /**
     * @param $varId
     * @param array $arrOptions
     * @return null|SimpleRecipesPropertiesModel
     */
	public static function findPropertiesById($varId, array $arrOptions=array())
	{
		$t = static::$strTable;

		return static::findByPk( $varId, $arrOptions);
	}	


}

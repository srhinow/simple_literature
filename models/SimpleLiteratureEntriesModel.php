<?php
namespace Srhinow\SimpleLiterature\Models;

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2019 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_literature
 * @license    LGPL
 * @filesource
 */


use Contao\Model;

class SimpleLiteratureEntriesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_literature_entries';

    /**
     * @param int $intLimit
     * @param int $intOffset
     * @param array $arrIds
     * @param array $arrOptions
     * @return Model\Collection|SimpleLiteratureEntriesModel|null
     */
    public static function findLiterature($intLimit=0, $intOffset=0, array $arrIds=array(), array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = null;

        if(is_array($arrIds) && count($arrIds) > 0)
        {
            $arrColumns = array("$t.id IN(" . implode(',', array_map('intval', $arrIds)) . ")");
        }

        if (!BE_USER_LOGGED_IN)
        {
            $arrColumns[] = "$t.published='1'";
        }

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.maintitle ASC";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;


        return static::findBy($arrColumns, null, $arrOptions);
    }
}
